//[SECTION] Dependencies and Modules
    const express = require('express');
    const mongoose = require('mongoose');
    const dotenv = require('dotenv');
    const userRoutes = require('./routes/users');
    const userProduct = require('./routes/products');
    const userOrder = require('./routes/orders');
    const cors = require('cors');


//[SECTION] Environment Setup
    dotenv.config(); 
    let account = process.env.CREDENTIALS
    const port = process.env.PORT;

//[SECTION] Server Setup
    const app = express();
    app.use(cors());
    app.use(express.json());



//[SECTION] Database Connection
    mongoose.connect(account);
    const connectStatus = mongoose.connection;
    connectStatus.once('open', () => console.log(`Database Connected`));


//[SECTION] Back end Routes
    app.use('/users',userRoutes)
    app.use('/products',userProduct)
    app.use('/orders', userOrder)


//[SECTION] Server Gateway Response
    app.get('/', (req, res) => {
        res.send('Welcome to Capstone-2 E-commerce')
    });
    app.listen(port, () => {
        console.log(`API is Hosted port ${port}`);
    });