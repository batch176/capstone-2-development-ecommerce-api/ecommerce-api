//[SECTION] Dependencies and Modules
   const User = require('../models/User');
   const bcrypt = require('bcrypt');
   const dotenv = require('dotenv');
   dotenv.config();
   const asin = Number(process.env.SALT)
   const auth = require('../auth')


//[SECTION] Functionalities [CREATE]
   module.exports.register = (userData) => {
      let email = userData.email;
      let passW = userData.password;


      let newUser = new User ({
          email: email,
          password: bcrypt.hashSync(passW, asin),
      });
       return newUser.save().then ((user, err) => {
           if (user) {
               return user;
           } else {
               return 'Failed to register account';
           };
       });
   }


//USER AUTHENTICATION --- LOGIN
module.exports.loginUser = (data) => {
    return User.findOne({ email: data.email}).then(result => {
        //If the user does not exist
        if (result == null) {
            return ("Email does not exist!");
        } else {
            //If the User exists
            const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

            //if the password match -- return token
            if(isPasswordCorrect){
                return {accessToken: auth.createAccessToken(result.toObject())}
            } else {
            //if the password do not match
                return ("Incorrect Password!");
            }
        }
    })
}

//Change regular user to admin

module.exports.changeUser = (userId) => {
    let updateUser = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(userId, updateUser).then((user, error) => {

        if(error){
            return ({message: "Update Error!"});
        } else {
            return ({message: "Update Success!"});
        }
    }).catch(error => error)
}



//[SECTION] --------------- RETRIEVE USER DETAILS
    module.exports.getProfile = (data) => {
        return User.findById(data).then(result => {
            //change the value of the user's password to an empty string
            result.password = '';
            return result;
        })
    }