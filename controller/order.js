//[SECTION] Dependencies and Modules
const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const req = require('express/lib/request');
const mongoose = require('mongoose');


module.exports.orderProduct = (buyerId, reqBody) => {

    //created a loop for earch item to add in data base
    let newOrder = new Order();

    newOrder.userId = buyerId

    reqBody.product.forEach(item => {
        newOrder.product.push(item)
    });


    //created a loop for multiplying quantity and price
    let totalPrice = 0;

    reqBody.product.forEach(item => {

    let subTotal = item.price * item.quantity
    totalPrice += subTotal;

    });

    newOrder.totalAmount = totalPrice



//save the created object to our database
return newOrder.save().then((order,error) => {
    //Course creation is successful or not
    if(error) {
        return false;
    } else {
        return ({message: "Order creation success!"});
    }
}).catch(error => ({message: "Problem with the order!"}))
}


//RETRIEVE ALL ORDER
module.exports.getAllOrder = () => {
    return Order.find({}).then(result => {
        return result;
    }).catch (error => error)
}

//RETRIEVE Authenticated Order
module.exports.getOrder = (userId) => {
    return Order.find({userId: userId}).then(result => {
        return result;
    }).catch (error => error)
}
