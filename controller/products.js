    //[SECTION] Dependencies and Modules
    const Product = require('../models/Product');
    const bcrypt = require('bcrypt');
    const dotenv = require('dotenv');
    const { model } = require('mongoose');
    dotenv.config();


//[SECTION] Functionalities [CREATE]
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });


//save the created object to our database
return newProduct.save().then((Product,error) => {
    //Course creation is successful or not
    if(error) {
        return false;
    } else {
        return true;
    }
}).catch(error => error)
}



//[SECTION] Functionalities [RETRIEVE]
module.exports.getAllProduct = () => {
    return Product.find({}).then(result => {
        return result;
    }).catch (error => error)
}


module.exports.getAllActive = () => {
    return Product.find({isActive:true}).then(result => {
        return result;
    }).catch (error => error)
}

module.exports.getProductById = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result
    }).catch (error => error)
}


//[SECTION] Functionalities [UPDATE]

module.exports.updateProduct = (productId, data) => {
	let newProduct = {
		name: data.name,
		description: data.description,
		price: data.price
    }

	return Product.findByIdAndUpdate(productId, newProduct).then((Product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//[SECTION] Functionalities [ARCHIVE]

module.exports.archiveProduct = (productId) => {
    let updatedProduct = {
        isActive: false
    };

    return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {

        if(error){
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}


//ACTIVATE A PRODUCT
    module.exports.ActivateProduct = (productId) => {
        let activateProduct = {
            isActive: true
        }
        return Product.findByIdAndUpdate(productId, activateProduct).then((product, error) => {
            if(error){
                return false;
            } else {
                return true;
            }
        }).catch(error => error)
    }