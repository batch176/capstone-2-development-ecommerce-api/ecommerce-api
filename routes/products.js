//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/products');
const ProductController = require('../controller/products')
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] Routes- POST
route.post('/create', verify, verifyAdmin, (req, res) => {
    ProductController.addProduct(req.body).then(result => res.send(result))
})


//[SECTION] Routes- GET

//get all product
route.get('/all', (req, res) => {
    ProductController.getAllProduct().then(result => res.send(result));
})

//get active product
route.get('/active', (req, res) => {
    ProductController.getAllActive().then(result => res.send(result));
})

//get product by ID
route.get('/:productId', (req, res) => {
    console.log(req.params.productId)
    ProductController.getProductById(req.params.productId).then(result => res.send(result));
})

//Update Product
route.put('/:updateProductId', verify, verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params.updateProductId, req.body).then(result => res.send(result))
})


//Archive Product
route.put('/:productId/Archive',verify, verifyAdmin, (req, res) => {
    ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
})


//Activate
route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
            ProductController.ActivateProduct(req.params.productId, req.body).then(
                resultFromController => res.send(resultFromController))
    });



//[SECTION] Expose Route System
module.exports = route; 
