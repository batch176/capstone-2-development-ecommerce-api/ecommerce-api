//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/order');
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//POST order
route.post('/order', auth.verify, (req, res) => {
 	controller.orderProduct(req.user.id, req.body).then(result => res.send(result));
})

//[RETRIEVE] all Order
route.get('/all', verify, verifyAdmin, (req, res) => {
    controller.getAllOrder().then(result => res.send(result));
})

//[RETRIEVE] authenticated user's id
route.get('/authenticatedUser', auth.verify, (req, res) => {
    controller.getOrder(req.user.id).then(result => res.send(result));
})




module.exports = route; 