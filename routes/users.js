//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/users');
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] Routes- POST
route.post('/register', (req, res) => {
    console.log(req.body);
    let userData = req.body;
    controller.register(userData).then(outcome=> {
        res.send(outcome);
    })

}); 

//[SECTION] Route for User Authentication(Login)
route.post('/login', (req, res) => {
	controller.loginUser(req.body).then(result => res.send(result));
})


route.put('/:userId/Archive', verify, verifyAdmin, (req, res) => {
    controller.changeUser(req.params.userId).then(result => res.send(result));
})

//[ROUTE] Routes - Get the users's details
route.get('/details', verify, (req, res) => {
    controller.getProfile(req.user.id).then(result => res.send(result));
})



//[SECTION] Expose Route System
module.exports = route; 
