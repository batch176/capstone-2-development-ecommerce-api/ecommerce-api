const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
    const orderSchema = new mongoose.Schema ({
        userId: {
            type: String,
            required: [true, 'Student ID is required']
        },
        product: [
            {
                productId: {
                    type: String,
                    required: [true, 'is required']
                },
                quantity: {
                    type: Number,
                    required: [true, 'is Required']
                },
            }
         ],
         totalAmount: {
            type: Number,
            required: [true, 'is Required']
         },
        purchasedOn: {
            type: Date,
            default: new Date()
         },
    });

//[SECTION] Model
    module.exports = mongoose.model('Order', orderSchema);